<?php

/**
 * Generation de carriere en fonction de la race
 */
function carriere(string $race){

  $dices = rand(1, 100);

  switch($race){

    case 'Humain':
      $carriere = carriereHumain($dices);
    break;

    case 'Halfling':
      $carriere = carriereHalfling($dices);
    break;

    case 'Nain':
      $carriere = carriereNain($dices);
    break;

    case 'Haut elfe':
      $carriere = carriereHautElfe($dices);
    break;

    case 'Elfe sylvain':
      $carriere = carriereElfeSylvain($dices);
    break;

  }

  $carriere = [
    'Classe' => $carriere[0],
    'Carriere' => $carriere[1]
  ];
  return $carriere;
}

/**
 * Renvoie un tableau avec une classe et une carrière
 */
function carriereHumain(int $dices){

  switch($dices){

    case 1:
      return ['Citadins','Agitateur'];
    break;

    case 2:
    case 3:
      return ['Citadins', 'Artisan'];
    break;

    case 4:
    case 5:
    case 6:
      return ['Citadins','Bourgeois'];
    break;

    case 7:
      return ['Citadins','Enqueteur'];
    break;

    case 8:
      return ['Citadins','Marchand'];
    break;

    case 9:
    case 10:
      return ['Citadins', 'Mendiant'];
    break;

    case 11:
      return ['Citadins', 'Milicien'];
    break;

    case 12:
    case 13:
      return ['Citadins', 'Ratier'];
    break;

    case 14:
      return ['Courtisans','Artiste'];
    break;

    case 15:
      return ['Courtisans', 'Conseiller'];
    break;

    case 16:
      return ['Courtisans', 'Duelliste'];
    break;

    case 17:
      return ['Courtisans', 'Emissaire'];
    break;

    case 18:
      return ['Courtisans', 'Espion'];
    break;

    case 19:
      return ['Courtisans', 'Intendant'];
    break;

    case 20:
      return ['Courtisans', 'Noble'];
    break;

    case 21:
    case 22:
    case 23:
      return ['Courtisans', 'Serviteur'];
    break;

    case 24:
    case 25:
      return['Guerriers', 'Cavalier'];
    break;

    case 26:
      return['Guerriers', 'Chevalier'];
    break;

    case 27:
    case 28:
      return['Guerriers', 'Garde'];
    break;

    case 29:
      return['Guerriers', 'Gladiateur'];
    break;

    case 30:
      return['Guerriers', 'Pretre guerrier'];
    break;

    case 31:
    case 32:
    case 33:
    case 34:
      return['Guerriers', 'Soldat'];
    break;

    case 35:
      return['Guerriers', 'Spadassin'];
    break;

    case 36:
      return['Itinerants', 'Chasseur de primes'];
    break;

    case 37:
      return['Itinerants', 'cocher'];
    break;

    case 38:
      return['Itinerants', 'Colporteur'];
    break;

    case 39:
    case 40:
      return['Itinerants', 'Flagellant'];
    break;

    case 41:
      return['Itinerants', 'Messager'];
    break;

    case 42:
      return['Itinerants', 'Patrouilleur routier'];
    break;

    case 43:
      return['Itinerants', 'Repurgateur'];
    break;

    case 44:
    case 45:
      return['Itinerants', 'Saltimbanque'];
    break;

    case 46:
      return['Lettrés', 'Apothicaire'];
    break;

    case 47:
    case 48:
      return['Lettrés', 'Erudit'];
    break;

    case 49:
      return['Lettrés', 'Ingenieur'];
    break;

    case 50:
      return['Lettrés', 'Juriste'];
    break;

    case 51:
      return['Lettrés', 'Medecin'];
    break;

    case 52:
    case 53:
      return['Lettrés', 'Nonne'];
    break;

    case 54:
    case 55:
    case 56:
    case 57:
    case 58:
      return['Lettrés', 'Pretre'];
    break;

    case 59:
      return['Lettrés', 'Sorcier'];
    break;

    case 60:
    case 61:
      return['Riverains', 'Batelier'];
    break;

    case 62:
      return['Riverains', 'Contrebandier'];
    break;

    case 63:
    case 64:
      return['Riverains', 'Débardeur'];
    break;

    case 65:
    case 66:
    case 67:
      return['Riverains', 'Femme du fleuve'];
    break;

    case 68;
    case 69;
      return['Riverains', 'Marin'];
    break;

    case 70:
      return['Riverains', 'Naufrageur'];
    break;

    case 71:
      return['Riverains', 'Nautonier'];
    break;

    case 72:
    case 73:
      return['Roublards', 'Patrouilleur fluvial'];
    break;

    case 74:
      return['Roublards', 'Charlattan'];
    break;

    case 75:
    case 76:
      return['Roublards', 'Entremetteur'];
    break;

    case 77:
    case 78:
    case 79:
    case 80:
      return['Roublards', 'Hors la loi'];
    break;

    case 81:
      return['Roublards', 'Pilleur de tombes'];
    break;

    case 82:
      return['Roublards', 'Rançonneur'];
    break;

    case 83:
      return['Roublards', 'Receleur'];
    break;

    case 84:
      return['Roublards', 'Sorcier sauvage'];
    break;

    case 85:
    case 86:
    case 87:
      return['Ruraux', 'Voleur'];
    break;


    case 88:
      return['Ruraux', 'Bailli'];
    break;


    case 89:
    case 90:
      return['Ruraux', 'Chasseur'];
    break;

    case 91:
      return['Ruraux', 'Eclaireur'];
    break;

    case 92:
      return['Ruraux', 'Herboriste'];
    break;

    case 93:
      return['Ruraux', 'Mineur'];
    break;

    case 94:
      return['Ruraux', 'Mystique'];
    break;

    case 95:
      return['Ruraux', 'Sorcier de village'];
    break;

    case 96:
    case 97:
    case 98:
    case 99:
    case 100:
      return['Ruraux', 'Villageois'];
    break;

  }
}

function carriereNain(int $dices){
  switch($dices){

    case 1:
    case 2:
      return ['Citadins','Agitateur'];
    break;

    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
      return ['Citadins', 'Artisan'];
    break;

    case 9:
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
      return ['Citadins','Bourgeois'];
    break;

    case 15:
    case 16:
      return ['Citadins','Enqueteur'];
    break;

    case 17:
    case 18:
    case 19:
    case 20:
      return ['Citadins','Marchand'];
    break;

    case 21:
      return ['Citadins', 'Mendiant'];
    break;

    case 22:
    case 23:
    case 24:
      return ['Citadins', 'Milicien'];
    break;

    case 25:
      return ['Citadins', 'Ratier'];
    break;

    case 26:
      return ['Courtisans','Artiste'];
    break;

    case 27:
    case 28:
      return ['Courtisans', 'Conseiller'];
    break;

    case 29:
      return ['Courtisans', 'Duelliste'];
    break;

    case 30:
    case 31:
      return ['Courtisans', 'Emissaire'];
    break;

    case 32:
      return ['Courtisans', 'Espion'];
    break;

    case 33:
    case 34:
      return ['Courtisans', 'Intendant'];
    break;

    case 35:
      return ['Courtisans', 'Noble'];
    break;

    case 36:
      return ['Courtisans', 'Serviteur'];
    break;

    case 37:
    case 38:
    case 39:
      return['Guerriers', 'Garde'];
    break;

    case 40:
    case 41:
    case 42:
      return['Guerriers', 'Gladiateur'];
    break;

    case 43:
    case 44:
    case 45:
      return['Guerriers', 'Soldat'];
    break;

    case 46:
    case 47:
    case 48:
      return['Guerriers', 'Spadassin'];
    break;

    case 49:
    case 50:
    case 51:
    case 52:
      return['Guerriers', 'Tueur'];
    break;

    case 53:
    case 54:
    case 55:
    case 56:
      return['Itinerants', 'Chasseur de primes'];
    break;

    case 57:
      return['Itinerants', 'cocher'];
    break;

    case 58:
    case 59:
      return['Itinerants', 'Colporteur'];
    break;

    case 60:
    case 61:
      return['Itinerants', 'Messager'];
    break;

    case 62:
    case 63:
      return['Itinerants', 'Saltimbanque'];
    break;

    case 64:
      return['Lettrés', 'Apothicaire'];
    break;

    case 65:
    case 66:
      return['Lettrés', 'Erudit'];
    break;

    case 67:
    case 68;
    case 69;
      return['Lettrés', 'Ingenieur'];
    break;

    case 70:
    case 71:
      return['Lettrés', 'Juriste'];
    break;

    case 72:
      return['Lettrés', 'Medecin'];
    break;

    case 73:
    case 74:
      return['Riverains', 'Batelier'];
    break;

    case 75:
    case 76:
      return['Riverains', 'Contrebandier'];
    break;

    case 77:
    case 78:
      return['Riverains', 'Débardeur'];
    break;

    case 79:
    case 80:
      return['Riverains', 'Femme du fleuve'];
    break;

    case 81:
      return['Riverains', 'Marin'];
    break;

    case 82:
      return['Riverains', 'Naufrageur'];
    break;

    case 83:
      return['Riverains', 'Nautonier'];
    break;

    case 84:
    case 85:
    case 86:
      return['Roublards', 'Hors la loi'];
    break;

    case 87:
      return['Roublards', 'Rançonneur'];
    break;

    case 88:
      return['Roublards', 'Receleur'];
    break;

    case 89:
      return['Ruraux', 'Voleur'];
    break;

    case 90:
    case 91:
      return['Ruraux', 'Bailli'];
    break;

    case 92:
    case 93:
      return['Ruraux', 'Chasseur'];
    break;

    case 94:
      return['Ruraux', 'Eclaireur'];
    break;

    case 95:
    case 96:
    case 97:
    case 98:
    case 99:
      return['Ruraux', 'Mineur'];
    break;

    case 100:
      return['Ruraux', 'Villageois'];
    break;

  }
}

function carriereHalfling(int $dices){

  switch($dices){

    case 1:
    case 2:
      return ['Citadins','Agitateur'];
    break;

    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
      return ['Citadins', 'Artisan'];
    break;

    case 8:
    case 9:
    case 10:
    return ['Citadins','Bourgeois'];
    break;

    case 11:
    case 12:
      return ['Citadins','Enqueteur'];
    break;

    case 13:
    case 14:
    case 15:
    case 16:
      return ['Citadins','Marchand'];
    break;

    case 17:
    case 18:
    case 19:
    case 20:
      return ['Citadins', 'Mendiant'];
    break;

    case 21:
    case 22:
      return ['Citadins', 'Milicien'];
    break;

    case 23:
    case 24:
    case 25:
      return ['Citadins', 'Ratier'];
    break;

    case 26:
    case 27:
      return ['Courtisans','Artiste'];
    break;

    case 28:
      return ['Courtisans', 'Conseiller'];
    break;

    case 29:
      return ['Courtisans', 'Emissaire'];
    break;

    case 30:
      return ['Courtisans', 'Espion'];
    break;

    case 31:
    case 32:
      return ['Courtisans', 'Intendant'];
    break;

    case 33:
    case 34:
    case 35:
    case 36:
    case 37:
    case 38:
      return ['Courtisans', 'Serviteur'];
    break;

    case 39:
    case 40:
      return['Guerriers', 'Garde'];
    break;

    case 41:
      return['Guerriers', 'Gladiateur'];
    break;

    case 42:
    case 43:
    case 44:
      return['Guerriers', 'Soldat'];
    break;

    case 45:
      return['Itinerants', 'Chasseur de primes'];
    break;

    case 46:
    case 47:
      return['Itinerants', 'cocher'];
    break;

    case 48:
    case 49:
      return['Itinerants', 'Colporteur'];
    break;

    case 50:
    case 51:
      return['Itinerants', 'Messager'];
    break;

    case 52:
      return['Itinerants', 'Patrouilleur routier'];
    break;

    case 53:
    case 54:
    case 55:
      return['Itinerants', 'Saltimbanque'];
    break;

    case 56:
      return['Lettrés', 'Apothicaire'];
    break;

    case 57:
    case 58:
      return['Lettrés', 'Erudit'];
    break;

    case 59:
      return['Lettrés', 'Ingenieur'];
    break;

    case 60:
    case 61:
      return['Lettrés', 'Juriste'];
    break;

    case 62:
    case 63:
      return['Lettrés', 'Medecin'];
    break;

    case 64:
      return['Riverains', 'Batelier'];
    break;

    case 65:
    case 66:
    case 67:
    case 68:
      return['Riverains', 'Contrebandier'];
    break;

    case 69:
    case 70:
    case 71:
      return['Riverains', 'Débardeur'];
    break;

    case 72:
    case 73:
    case 74:
      return['Riverains', 'Femme du fleuve'];
    break;

    case 75:
      return['Riverains', 'Marin'];
    break;

    case 76:
      return['Riverains', 'Nautonier'];
    break;

    case 77:
      return['Roublards', 'Patrouilleur fluvial'];
    break;

    case 78:
      return['Roublards', 'Charlattan'];
    break;

    case 79:
    case 80:
    case 81:
      return['Roublards', 'Entremetteur'];
    break;

    case 82:
      return['Roublards', 'Hors la loi'];
    break;

    case 83:
      return['Roublards', 'Pilleur de tombes'];
    break;

    case 84:
      return['Roublards', 'Rançonneur'];
    break;

    case 85:
      return['Roublards', 'Receleur'];
    break;

    case 86:
    case 87:
    case 88:
    case 89:
      return['Ruraux', 'Voleur'];
    break;

    case 90:
      return['Ruraux', 'Bailli'];
    break;

    case 91:
    case 92:
      return['Ruraux', 'Chasseur'];
    break;

    case 93:
      return['Ruraux', 'Eclaireur'];
    break;

    case 94:
    case 95:
    case 96:
      return['Ruraux', 'Herboriste'];
    break;

    case 97:
      return['Ruraux', 'Mineur'];
    break;

    case 98:
    case 99:
    case 100:
      return['Ruraux', 'Villageois'];
    break;

  }
}

function carriereHautElfe(int $dices){

  switch($dices) {

    case 1:
    case 2:
    case 3:
      return ['Citadins', 'Artisan'];
    break;

    case 4:
    case 5:
      return ['Citadins','Bourgeois'];
    break;

    case 6:
    case 7:
      return ['Citadins','Enqueteur'];
    break;

    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
      return ['Citadins','Marchand'];
    break;

    case 13:
      return ['Citadins', 'Milicien'];
    break;

    case 14:
      return ['Courtisans','Artiste'];
    break;

    case 15:
    case 16:
      return ['Courtisans', 'Conseiller'];
    break;

    case 17:
    case 18:
      return ['Courtisans', 'Duelliste'];
    break;

    case 19:
    case 20:
    case 21:
      return ['Courtisans', 'Emissaire'];
    break;

    case 22:
    case 23:
    case 24:
      return ['Courtisans', 'Espion'];
    break;

    case 25:
    case 26:
      return ['Courtisans', 'Intendant'];
    break;

    case 27:
    case 28:
    case 29:
      return ['Courtisans', 'Noble'];
    break;

    case 30:
    case 31:
    case 32:
    case 33:
      return['Guerriers', 'Cavalier'];
    break;

    case 34:
      return['Guerriers', 'Chevalier'];
    break;

    case 35:
    case 36:
      return['Guerriers', 'Garde'];
    break;

    case 37:
    case 38:
      return['Guerriers', 'Gladiateur'];
    break;

    case 39:
    case 40:
      return['Guerriers', 'Soldat'];
    break;

    case 41:
      return['Guerriers', 'Spadassin'];
    break;

    case 42:
    case 43:
    case 44:
      return['Itinerants', 'Chasseur de primes'];
    break;

    case 45:
      return['Itinerants', 'Messager'];
    break;

    case 46:
    case 47:
    case 48:
      return['Itinerants', 'Saltimbanque'];
    break;

    case 49:
    case 50:
      return['Lettrés', 'Apothicaire'];
    break;

    case 51:
    case 52:
    case 53:
    case 54:
      return['Lettrés', 'Erudit'];
    break;

    case 55:
    case 56:
    case 57:
    case 58:
      return['Lettrés', 'Juriste'];
    break;

    case 59:
    case 60:
      return['Lettrés', 'Medecin'];
    break;

    case 61:
    case 62:
    case 63:
    case 64:
      return['Lettrés', 'Sorcier'];
    break;

    case 65:
      return['Riverains', 'Batelier'];
    break;

    case 66:
      return['Riverains', 'Contrebandier'];
    break;

    case 67:
    case 68;
    case 69;
    case 70:
    case 71:
    case 72:
    case 73:
    case 74:
    case 75:
    case 76:
    case 77:
    case 78:
    case 79:
    case 80:
    case 81:
      return['Riverains', 'Marin'];
    break;

    case 82:
    case 83:
    case 84:
      return['Roublards', 'Charlattan'];
    break;

    case 85:
    case 86:
      return['Roublards', 'Entremetteur'];
    break;

    case 87:
    case 88:
    case 89:
      return['Roublards', 'Hors la loi'];
    break;

    case 90:
    case 91:
    case 92:
      return['Ruraux', 'Chasseur'];
    break;

    case 93:
    case 94:
    case 95:
    case 96:
    case 97:
    case 98:
      return['Ruraux', 'Eclaireur'];
    break;

    case 99:
    case 100:
      return['Ruraux', 'Herboriste'];
    break;

  }
}

function carriereElfeSylvain(int $dices){
  switch($dices) {

    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      return ['Citadins', 'Artisan'];
    break;

    case 6:
    case 7:
    case 8:
    case 9:
      return ['Courtisans','Artiste'];
    break;

    case 10:
    case 11:
    case 12:
    case 13:
      return ['Courtisans', 'Conseiller'];
    break;

    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
    case 20:
      return ['Courtisans', 'Emissaire'];
    break;

    case 21:
    case 22:
    case 23:
    case 24:
      return ['Courtisans', 'Espion'];
    break;

    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
    case 30:
      return ['Courtisans', 'Noble'];
    break;

    case 31:
    case 32:
    case 33:
    case 34:
    case 35:
      return['Guerriers', 'Cavalier'];
    break;

    case 36:
    case 37:
      return['Guerriers', 'Chevalier'];
    break;

    case 38:
    case 39:
      return['Guerriers', 'Garde'];
    break;

    case 40:
    case 41:
      return['Guerriers', 'Gladiateur'];
    break;

    case 42:
    case 43:
    case 44:
    case 45:
      return['Guerriers', 'Soldat'];
    break;

    case 46:
    case 47:
      return['Itinerants', 'Chasseur de primes'];
    break;

    case 48:
    break;

    case 49:
    case 50:
      return['Itinerants', 'Messager'];
    break;

    case 51:
    case 52:
    case 53:
    case 54:
    case 55:
      return['Itinerants', 'Saltimbanque'];
    break;

    case 56:
      return['Lettrés', 'Erudit'];
    break;

    case 57:
    case 58:
    case 59:
    case 60:
      return['Lettrés', 'Sorcier'];
    break;

    case 61:
      return['Riverains', 'Naufrageur'];
    break;

    case 62:
    case 63:
    case 64:
    case 65:
    case 66:
    case 67:
      return['Roublards', 'Hors la loi'];
    break;

    case 68;
    case 69;
    case 70:
    case 71:
    case 72:
    case 73:
    case 74:
    case 75:
    case 76:
    case 77:
      return['Ruraux', 'Chasseur'];
    break;

    case 78:
    case 79:
    case 80:
    case 81:
    case 82:
    case 83:
    case 84:
    case 85:
    case 86:
    case 87:
    case 88:
      return['Ruraux', 'Eclaireur'];
    break;

    case 89:
    case 90:
    case 91:
    case 92:
    case 93:
    case 94:
    case 95:
      return['Ruraux', 'Herboriste'];
    break;

    case 96:
    case 97:
    case 98:
    case 99:
    case 100:
      return['Ruraux', 'Mystique'];
    break;

  }
}
