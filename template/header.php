<?php
  require_once '../src/functions.php';
  require_once '../src/carriere.php';
  require_once '../src/attributs.php';
?>

<!DOCTYPE html>

<html lang="fr">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Le Quidam</title>

    <!-- favicon -->
    <link rel="shortcut icon" href="../public/img/die20.png">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
      integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../public/css/style.css">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">

      <a class="navbar-brand" href="../index.php"><img id="logo" src="../public/img/die20.png"> Le Quidam</a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">

        <ul class="navbar-nav ml-auto">
          <?php $class = 'nav-link'?>
          <?= menu('/template/generateur.php', 'Générateur', $class); ?>
          <!-- menu('/template/propos.php', 'À propos', $class); -->
        </ul>

      </div>

    </nav>
