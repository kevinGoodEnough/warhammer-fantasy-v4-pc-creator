<?php
  require_once 'src/functions.php';
  require_once 'src/carriere.php';
  require_once 'src/attributs.php';
?>

<!DOCTYPE html>

<html lang="fr">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Le Quidam</title>

    <!-- favicon -->
    <link rel="shortcut icon" href="public/img/die20.png">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
      integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="public/css/style.css">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">

      <a class="navbar-brand" href="index.php"><img id="logo" src="public/img/die20.png"> Le Quidam</a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">

        <ul class="navbar-nav ml-auto">
          <?php $class = 'nav-link'?>
          <?= menu('/template/generateur.php', 'Générateur', $class); ?>
          <!-- menu('/template/propos.php', 'À propos', $class);  -->
        </ul>

      </div>

    </nav>

    <div class="container">
      <div class="row justify-content-center">

        <div id="test" class="col-10">
          <h5>Bienvenu sur Le Quidam !</h5>
          <p> Ce site a pour but de générer un personnage aléatoire en suivant les règles du jeu de rôle <span id="warhammer"> Warhammer fantasy V4</span>.</p>
          <h5>Comment on s'en sert ?</h5>
          <p>
            Rien de plus simple ! Il suffit de cliquer sur le bouton "générateur" en haut de la page et un tout nouveau personnage sera généré automatiquement.
            Si celui-ci ne vous plait pas, pas de panique, il vous suffit d'actualiser la page ou bien de cliquer à nouveau sur le bouton pour en obtenir un tout frais !
          </p>
          <h5>Comment ça marche ?</h5>
          <p>L'application selectionne un nombre entre 1 et 100 et choisit une race grâce au nombre obtenu.</p>

            <table class="table table-bordered text-center sm">

              <tbody>
                <td>De 1 à 90</td>
                <td> Humain</td>
              </tbody>

              <tbody>
                <td>De 91 à 95</td>
                <td>Halfling</td>
              </tbody>

              <tbody>
                <td>De 94 à 98</td>
                <td>Nain</td>
              </tbody>

              <tbody>
                <td>99</td>
                <td>Haut elfe</td>
              </tbody>

              <tbody>
                <td>100</td>
                <td>Elfe sylvain</td>
              </tbody>

            </table>

          <p>
            Ensuite un nouveau nombre entre 1 et 100 est tiré. En fonction de la race et du nombre obtenu, une carrière est attribuée.
            Par exemple, seul un nain peut embrasser la carrière de tueur de troll !<br>
            À noter que la classe est liée à la carrière. On trouve dans la classe des guerriers, les carrières de soldats, paladins, gladiateurs etc...
          </p>
          <p>
            Le dernier point et non des moindres, la génération des attributs.
            Ils sont calculés en fonction de la race. Prenons par exemple l'attribut "Capacité de combat".
            Pour un personnage de type humain le calcul se fera de cette façon : <br>
            Génération et addition de deux nombres entre 1 et 10 puis ajout d'une valeur brute de 20 pour un résultat minimum de 22 et maximum de 40. <br>
            Pour un nain ce calcul sera légérement différent : <br>
            Génération et addition de deux nombres entre 1 et 10 puis ajout d'une valeur brute de 40 pour un résultat minimum de 42 et maximum de 60. <br>
            On se rend rapidement compte que la race naine est naturellement plus efficace dans cet attribut !
          </p>

          <h5>Pour finir...</h5>
          <p>
            Terminer la création de votre personnage nécessite le manuel des règles de la 4eme édition de <span id="warhammer">Warhammer Fantasy</span>.
          </p>

        </div>
      </div>
    </div>

    <div>
      <!-- Bootstrap core JavaScript -->
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
      </script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
      </script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
      </script>
    </div>

  </body>

</html>
