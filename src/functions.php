<?php

/**
 * Lance les étapes successivent de la création de personnage
 */
function creation(){

  $race = race();

  $personnage = [
      'Race' => $race,
      'Carriere' => carriere($race),
      'Attributs' => attributs($race)
  ];

    return($personnage);
}

/**
 * Renvoie une race sous forme de chaine en fonction d'un resultat aleatoire entre 1 et 100.
 */
function race() {

  $dices = rand(1, 100);

  switch ($dices) {

    case $dices <= 90:
      return 'Humain';
    break;

    case $dices > 90 && $dices <= 95:
      return 'Halfling';
    break;

    case $dices > 95 && $dices <= 98:
      return 'Nain';
    break;

    case $dices === 99:
      return 'Haut elfe';
    break;

    default:
      return 'Elfe sylvain';
    break;
  }
}

/**
 *Génère une image particulière en fonction de la race du personnage
 */
function raceImage($race){

  switch($race){

    case 'Humain':
      return '<img class="rounded mx-auto d-block" id="imgPerso" src="/public/img/humain.jpg" alt="Card image cap">';
    break;

    case 'Halfling':
      return '<img class="rounded mx-auto d-block" id="imgPerso" src="/public/img/halfing.jpg" alt="Card image cap">';
    break;

    case 'Nain':
      return '<a href="https://www.bobkehl.com/" ><img class="rounded mx-auto d-block" id="imgPerso" src="/public/img/nain.jpg" alt="Card image cap"> </a>';
    break;

    case 'Haut elfe':
      return '<img class="rounded mx-auto d-block" id="imgPerso" src="/public/img/hautElfe.jpg" alt="Card image cap">';
    break;

    case 'Elfe sylvain':
      return '<img class="rounded mx-auto d-block" id="imgPerso" src="/public/img/elfeSylvain.jpg" alt="Card image cap">';
    break;

  }
}

/**
 * Créer un element de liste et utilise $_SERVEUR pour insérer la class "active" à un élément de liste
 */
function menu(string $lien, string $titre, string $linkClass = ''){

  $classe = 'nav-item';

  if($_SERVER['SCRIPT_NAME'] === $lien){
    $classe .= ' active';
  }

  return <<<HTML
  <li class="$classe">
    <a class="nav-link" href="$lien">$titre</a>
  </li>
HTML;
}
