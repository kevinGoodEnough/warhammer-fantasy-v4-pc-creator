<?php

/**
 * Calcul un jet de dé en prennant en parametre le nombre de dé, le type (6, 10, 12, 20) et ajoute un montant arbitraire.
 */
function calculAttributs(int $nombreDe, int $typeDeDe, int $ajout) {

  $total = $nombreDe + (rand(1, $typeDeDe) + rand(1, $typeDeDe)) + $ajout;
  return $total;

}


/**
 * Calcul les points de blessures (points de vie max)
 */
function pointsDeBlessure(string $race, int $force, int $endu, int $forceMental){

  $bonusForce = substr($force,0 , 1);
  $bonusEndu = substr($endu,0 , 1);
  $bonusForceMental = substr($forceMental,0 , 1);

  if($race === 'Halfling'){
    $ptsBlessure = (2 * $bonusEndu) + $bonusForceMental;
    return $ptsBlessure;

  } else {
    $ptsBlessure = ($bonusForce + (2 * $bonusEndu) + $bonusForceMental);
    return $ptsBlessure;
  }

}

// Utilise calculAttributs() pour générer un tableau de de stat en fonction de la race du personnage.
function attributs(string $race) {

  switch($race){

    case 'Humain':

      $attributs = [
        'Capacite de combat' => calculAttributs(2, 10, 20),
        'Capacite de tir' => calculAttributs(2, 10, 20),
        'Force' => calculAttributs(2, 10, 20),
        'Endurance' => calculAttributs(2, 10, 20),
        'Agilite' => calculAttributs(2, 10, 20),
        'Initiative' => calculAttributs(2, 10, 20),
        'Dexterite' => calculAttributs(2, 10, 20),
        'Intelligence' => calculAttributs(2, 10, 20),
        'Force mentale' => calculAttributs(2, 10, 20),
        'Sociabilite' => calculAttributs(2, 10, 20),
        'Points de blessure' => null,
        'Points de destin' => 2,
        'Resilience' => 1,
        'Points supplementaires' => 3,
        'Mouvement' => 4
      ];
    break;

    case 'Nain':

      $attributs = [
        'Capacite de combat' => calculAttributs(2, 10, 30),
        'Capacite de tir' => calculAttributs(2, 10, 20),
        'Force' => calculAttributs(2, 10, 20),
        'Endurance' => calculAttributs(2, 10, 30),
        'Agilite' => calculAttributs(2, 10, 10),
        'Initiative' => calculAttributs(2, 10, 20),
        'Dexterite' => calculAttributs(2, 10, 30),
        'Intelligence' => calculAttributs(2, 10, 20),
        'Force mentale' => calculAttributs(2, 10, 40),
        'Sociabilite' => calculAttributs(2, 10, 10),
        'Points de blessure' => null,
        'Points de destin' => 0,
        'Resilience' => 2,
        'Points supplementaires' => 2,
        'Mouvement' => 3
      ];
    break;

    case 'Halfling':

      $attributs = [
        'Capacite de combat' => calculAttributs(2, 10, 10),
        'Capacite de tir' => calculAttributs(2, 10, 30),
        'Force' => calculAttributs(2, 10, 10),
        'Endurance' => calculAttributs(2, 10, 20),
        'Agilite' => calculAttributs(2, 10, 20),
        'Initiative' => calculAttributs(2, 10, 20),
        'Dexterite' => calculAttributs(2, 10, 30),
        'Intelligence' => calculAttributs(2, 10, 20),
        'Force mentale' => calculAttributs(2, 10, 30),
        'Sociabilite' => calculAttributs(2, 10, 30),
        'Points de blessure' => null,
        'Points de destin' => 0,
        'Resilience' => 2,
        'Points supplementaires' => 3,
        'Mouvement' => 3
      ];
    break;

    case 'Haut elfe':
    case 'Elfe sylvain':

      $attributs = [
        'Capacite de combat' => calculAttributs(2, 10, 30),
        'Capacite de tir' => calculAttributs(2, 10, 30),
        'Force' => calculAttributs(2, 10, 20),
        'Endurance' => calculAttributs(2, 10, 20),
        'Agilite' => calculAttributs(2, 10, 30),
        'Initiative' => calculAttributs(2, 10, 40),
        'Dexterite' => calculAttributs(2, 10, 30),
        'Intelligence' => calculAttributs(2, 10, 30),
        'Force mentale' => calculAttributs(2, 10, 30),
        'Sociabilite' => calculAttributs(2, 10, 20),
        'Points de blessure' => null,
        'Points de destin' => 0,
        'Resilience' => 0,
        'Points supplementaires' => 2,
        'Mouvement' => 5
      ];
    break;

  }

  $pointsDeBlessure = pointsDeBlessure($race, $attributs['Force'], $attributs['Endurance'], $attributs['Force mentale']);
  $attributs['Points de blessure'] = "$pointsDeBlessure";
  return $attributs;

}
