<?php
  require '../template/header.php';
  $personnage = creation();
  ?>

<div id="contenu" class="container">

  <row class="row text-center">

    <div class="col-md">
      <h4> Race : <?= $personnage['Race']?> </h4>
    </div>

    <?php foreach($personnage['Carriere'] as $key=>$value): ?>
      <div class="col-md">
        <h4> <?= $key; ?> : <?= $value; ?> </h4>
      </div>
    <?php endforeach ?>

  </row>

  <div class="row justify-content-center" id="blockImgStat">

    <div class="col-lg-5">
      <?= raceImage($personnage['Race']); ?>
    </div>

    <div class="col-lg-5">

      <table class="table table-sm">
        <tbody>
          <?php foreach($personnage['Attributs'] as $key=>$value): ?>
            <tr>
              <th scope="row"> <?= $key; ?> </th>
              <td> <?= $value; ?> </td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>

    </div>
  </div>
</div>
