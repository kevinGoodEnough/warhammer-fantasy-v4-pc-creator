:warning: 

Currently learning programming, i work on a player charactere generator in php.
I dont know if i will finish it some day but i will try my best for it. ;)


### What do you need?

PHP 7.x

### How i use it? (maj 28/05/2020)

Clone the repository and open a php server at the root location with this command:

php -S localhost:8000

Open your browser and go to localhost:8000. 

For generate a new charactere just reload the page.

Enjoy!

### Old version: (08/05/2020) (dd/MM/YYYY)

```cli
------------------------------------------------------------------
Bienvenue sur la création de personnage pour Warhammer fantasy V4.
------------------------------------------------------------------

        [1] Creation de personnage aléatoire
        [2] Processus de création
        [q] Quitter

Saisissez une entrée: 1
------------------------------------------------------------------
Calcul des attributs...
2d10 + 20 = 27 
2d10 + 20 = 31 
2d10 + 20 = 26 
2d10 + 20 = 33 
2d10 + 20 = 39 
2d10 + 20 = 39 
2d10 + 20 = 36 
2d10 + 20 = 32 
2d10 + 20 = 31 
2d10 + 20 = 36 
Array
(
    [race] => Humain
    [carriere] => Array
        (
            [Classe] => Lettrés
            [Carriere] => Erudit
        )

    [attributs] => Array
        (
            [capacite de combat] => 27
            [capacite de tir] => 31
            [force] => 26
            [Endurance] => 33
            [agilite] => 39
            [initiative] => 39
            [dexterite] => 36
            [intelligence] => 32
            [force mentale] => 31
            [sociabilite] => 36
            [points de blessure] => 20
            [points de destin] => 2
            [resilience] => 1
            [points supplementaires] => 3
            [mouvement] => 4
        )

)
```